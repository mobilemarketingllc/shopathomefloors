var $menu = jQuery('.searchIcon, .searchBar');

jQuery(document).mouseup(function(e) {
    if (!$menu.is(e.target) && $menu.has(e.target).length === 0 && jQuery('.searchBar').css('display') == 'block') {
        jQuery('.searchBar').slideToggle();
    }
});

jQuery('.searchIcon .fl-icon-wrap .fl-icon').click(function() {

    jQuery('.searchBar').slideToggle();

});

jQuery(document).ready(function() {
    jQuery(".getcoupon-btn, .responsive-button-wrapper-default a.button.alt").attr("href", "/shop-at-home/");
    jQuery(" .getcoupon-btn , .responsive-button-wrapper-default a.button.alt").text("SCHEDULE A FREE IN HOME ESTIMATE");
});